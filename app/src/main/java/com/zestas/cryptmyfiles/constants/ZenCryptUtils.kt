package com.zestas.cryptmyfiles.constants

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.lifecycleScope
import com.zestas.cryptmyfiles.dataItemModels.ZenCryptSettingsModel
import kotlinx.coroutines.launch
import java.io.File


class ZenCryptUtils {
    companion object {
        private const val ENCRYPTED = "encrypted"
        private const val DECRYPTED = "decrypted"
        const val ACTION_CODE = "ACTION_CODE"
        const val REQUEST_CODE = "REQUEST_CODE"
        const val REPLACE_CODE = "REPLACE_CODE"
        const val FILE = "FILE"
        const val FINGERPRINT_KEYSTORE_ALIAS = "secure_fingerprint_pass"
        const val FROM_RECEIVED_URI: Int = -4
        const val FILE_PICK_MULTIPLE : Int = -3
        const val FILE_PICK : Int = -2
        const val FROM_CARD_VIEW : Int = -1
        const val ACTION_ENCRYPT : Int = 0
        const val ACTION_ENCRYPT_MULTIPLE = 1
        const val ACTION_DECRYPT : Int = 2
        const val ACTION_DECRYPT_MULTIPLE = 3

        const val REPLACE_WITH_ENCRYPTED: Int = 4
        const val REPLACE_WITH_DECRYPTED: Int = 5

        const val ZENCRYPT_VERSION_4_3 = 281

        @Throws(NullPointerException::class)
        fun encryptedFilesDirExternal(activity: AppCompatActivity): DocumentFile {
            var isDifferentStorageSelectedAndValid = false
            if (!isCustomDirectoryUriValid(activity)) {
                activity.lifecycleScope.launch {
                    ZenCryptSettingsModel.useDifferentDirectory.update(false)
                    ZenCryptSettingsModel.differentDirectoryUri.update("null")
                }
            } else
                isDifferentStorageSelectedAndValid = true


            return if (isDifferentStorageSelectedAndValid) {
                val uri = Uri.parse(ZenCryptSettingsModel.differentDirectoryUri.value)
                activity.contentResolver.takePersistableUriPermission(
                    uri,
                    Intent.FLAG_GRANT_READ_URI_PERMISSION or
                            Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                )
                val treeDocumentFile = DocumentFile.fromTreeUri(activity, uri)

                var encryptedFilesDir = treeDocumentFile?.findFile(ENCRYPTED)
                if (encryptedFilesDir == null)
                    encryptedFilesDir = treeDocumentFile!!.createDirectory(ENCRYPTED)

                encryptedFilesDir ?: throw NullPointerException()
            } else
                throw NullPointerException()
        }

        @Throws(NullPointerException::class)
        fun encryptedFilesDirInternal(context: Context) : File {
            return context.getExternalFilesDir(File.separator + ENCRYPTED)
                ?: throw NullPointerException()
        }

        @Throws(NullPointerException::class)
        fun decryptedFilesDirExternal(activity: AppCompatActivity): DocumentFile {
            var isDifferentStorageSelectedAndValid = false
            if (!isCustomDirectoryUriValid(activity)) {
                activity.lifecycleScope.launch {
                    ZenCryptSettingsModel.useDifferentDirectory.update(false)
                    ZenCryptSettingsModel.differentDirectoryUri.update("null")
                }
            } else
                isDifferentStorageSelectedAndValid = true

            return if (isDifferentStorageSelectedAndValid) {
                val uri = Uri.parse(ZenCryptSettingsModel.differentDirectoryUri.value)
                activity.contentResolver.takePersistableUriPermission(
                    uri,
                    Intent.FLAG_GRANT_READ_URI_PERMISSION or
                            Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                )
                val treeDocumentFile = DocumentFile.fromTreeUri(activity, uri)

                var encryptedFilesDir = treeDocumentFile?.findFile(DECRYPTED)
                if (encryptedFilesDir == null)
                    encryptedFilesDir = treeDocumentFile!!.createDirectory(DECRYPTED)

                encryptedFilesDir ?: throw NullPointerException()
            } else
                throw NullPointerException()
        }

        @Throws(NullPointerException::class)
        fun decryptedFilesDirInternal(context: Context): File {
            return context.getExternalFilesDir(File.separator + DECRYPTED)
                ?: throw NullPointerException()
        }

        private fun isCustomDirectoryUriValid(context: Context): Boolean {
            val treeDocumentFile = DocumentFile.fromTreeUri(context, Uri.parse(ZenCryptSettingsModel.differentDirectoryUri.value))
            return treeDocumentFile!!.exists() && treeDocumentFile.isDirectory
        }

        fun isUsingCustomDirectory(): Boolean {
            return (ZenCryptSettingsModel.useDifferentDirectory.value && ZenCryptSettingsModel.differentDirectoryUri.value != "null")
        }

        fun getIterations() : Int {
            return ZenCryptSettingsModel.iterationsNumber.value
        }
    }
}