package com.zestas.cryptmyfiles.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.documentfile.provider.DocumentFile
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.zestas.cryptmyfiles.R
import com.zestas.cryptmyfiles.adapters.EncryptedFilesExpandableRecyclerAdapter
import com.zestas.cryptmyfiles.constants.ZenCryptUtils
import com.zestas.cryptmyfiles.dataItemModels.DocumentFileItem
import com.zestas.cryptmyfiles.dataItemModels.FileItem
import com.zestas.cryptmyfiles.dataItemModels.ZenCryptSettingsModel
import com.zestas.cryptmyfiles.databinding.FragmentEncryptedViewBinding
import com.zestas.cryptmyfiles.helpers.ExitHelper.Companion.exit
import com.zestas.cryptmyfiles.helpers.FileSearchHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import kotlin.system.exitProcess


class EncryptedViewFragment : Fragment(R.layout.fragment_encrypted_view) {
    //---
    private val binding by viewBinding(FragmentEncryptedViewBinding::bind)
    private lateinit var progressDialog: AlertDialog
    private var externalFilesDirDocumentFile: DocumentFile? = null
    private var externalFilesDirFile: File? = null
    private var dataDocFileItems: ArrayList<DocumentFileItem>? = null
    private var dataFileItems: ArrayList<FileItem>? = null
    private lateinit var adapter: EncryptedFilesExpandableRecyclerAdapter
    //---

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // reset all variables to null, in case of a fragment redraw.
        // otherwise, these would be set only once, on application open.
        resetVars()

        if (ZenCryptUtils.isUsingCustomDirectory()) {
            try {
                externalFilesDirDocumentFile = ZenCryptUtils.encryptedFilesDirExternal(requireActivity() as AppCompatActivity)
                binding.tvLocation.text = externalFilesDirDocumentFile!!.uri.path.toString()
            } catch (e: NullPointerException) {
                Toast.makeText(activity,"Custom directory location not found. Reverting to default.", Toast.LENGTH_LONG).show()
                externalFilesDirDocumentFile = null
            }
        }

        if (externalFilesDirDocumentFile == null) {
            try {
                externalFilesDirFile = ZenCryptUtils.encryptedFilesDirInternal(requireContext())
                binding.tvLocation.text = externalFilesDirFile!!.absolutePath
            } catch (e: NullPointerException) {
                Toast.makeText(requireActivity(), "Storage media not mounted! Please restart the app.", Toast.LENGTH_SHORT).show()
                exit(requireActivity())
                exitProcess(-1)
            }
        }

        loadDataAndPopulateCardView()
        initToolbarMenu()
    }

    private fun loadDataAndPopulateCardView() {
        buildProgressDialog()
        progressDialog.show()
        lifecycleScope.launch {
            if (externalFilesDirDocumentFile != null) {
                dataDocFileItems = withContext(Dispatchers.IO) {
                    val encryptedDocumentFileItems: ArrayList<DocumentFileItem> = ArrayList()
                    externalFilesDirDocumentFile!!.listFiles().filter { file -> !file.isDirectory }
                        .sortedWith(
                            if (ZenCryptSettingsModel.sorting.value == 0) compareByDescending { it.lastModified() }
                            else compareBy { it.name }
                        ).forEach { file ->
                            if (file.name!!.endsWith(ZenCryptSettingsModel.extension.value))
                                encryptedDocumentFileItems.add(DocumentFileItem.create(file))
                        }
                    return@withContext encryptedDocumentFileItems
                }
            } else {
                dataFileItems = withContext(Dispatchers.IO) {
                    val encryptedFileItems: ArrayList<FileItem> = ArrayList()
                    externalFilesDirFile!!.walkTopDown().filter { file -> !file.isDirectory }
                        .sortedWith(
                            if (ZenCryptSettingsModel.sorting.value == 0) compareByDescending { it.lastModified() }
                            else compareBy { it.name }
                        ).forEach { file ->
                            if (file.name.endsWith(ZenCryptSettingsModel.extension.value))
                                encryptedFileItems.add(FileItem.create(file))
                        }
                    return@withContext encryptedFileItems
                }
            }

            val tvLocation = binding.tvLocation
            // the below works, because if one of them is null, then:
            // null == 0 is always FALSE.
            // Thus, we can use data?.size == 0 on both arraylists with || (OR)
            if (dataFileItems?.size == 0 || dataDocFileItems?.size == 0) {
                val emptyListView = binding.emptyList
                val infoIcon = binding.infoIcon
                emptyListView.visibility = VISIBLE
                tvLocation.visibility = GONE
                infoIcon.visibility = VISIBLE
            } else {
                tvLocation.visibility = VISIBLE
                val recyclerView: RecyclerView = binding.cardListRecyclerView
                recyclerView.setHasFixedSize(true)
                val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
                recyclerView.layoutManager = layoutManager
                adapter = EncryptedFilesExpandableRecyclerAdapter(dataFileItems, dataDocFileItems)
                recyclerView.adapter = adapter
                val extendedFloatingActionButton =
                    super.getActivity()?.findViewById<ExtendedFloatingActionButton>(R.id.fab)
                recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrolled(
                        recyclerView: RecyclerView,
                        dx: Int,
                        dy: Int
                    ) {
                        if (dy > 0 && extendedFloatingActionButton?.isExtended == true) {
                            extendedFloatingActionButton.shrink()
                        } else if (dy < 0 && extendedFloatingActionButton?.isExtended == false) {
                            extendedFloatingActionButton.extend()
                        }
                    }
                })
            }

            progressDialog.dismiss()
        }
    }

    private fun initToolbarMenu() {
        val toolbar = requireActivity().findViewById<MaterialToolbar>(R.id.toolbar)
        toolbar.setOnMenuItemClickListener {
            val searchView: SearchView
            when(it.itemId) {
                R.id.search_files -> {
                    searchView = it.actionView as SearchView
                    searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
                        android.widget.SearchView.OnQueryTextListener {
                        override fun onQueryTextSubmit(p0: String?): Boolean {
                            return false
                        }

                        override fun onQueryTextChange(msg: String): Boolean {
                            // inside on query text change method we are
                            // calling a method to filter our recycler view.

                            // Here, we can't use the same trick as above:
                            // data?.size != 0, because if one of the is null, then:
                            // null != 0 is always TRUE.
                            if (dataFileItems != null && dataFileItems?.size != 0)
                                FileSearchHelper.filterResults(dataFileItems!!, msg, adapter)
                            else if (dataDocFileItems != null && dataDocFileItems?.size != 0)
                                FileSearchHelper.filterResults(dataDocFileItems!!, msg, adapter)
                            return false
                        }
                    })
                    true
                }
                else -> false
            }
        }
    }

    override fun onStop() {
        super.onStop()
        if (progressDialog.isShowing)
            progressDialog.dismiss()
    }

    override fun onDestroyView() {
        val recyclerView: RecyclerView = binding.cardListRecyclerView
        recyclerView.adapter = null
        super.onDestroyView()
    }


    private fun buildProgressDialog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setCancelable(false)

        builder.setView(R.layout.indeterminate_progress_circular)
        progressDialog = builder.create()
    }

    private fun resetVars() {
        externalFilesDirDocumentFile = null
        externalFilesDirFile = null
        dataFileItems = null
        dataDocFileItems = null
    }
}