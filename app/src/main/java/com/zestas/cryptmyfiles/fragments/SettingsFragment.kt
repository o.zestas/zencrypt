package com.zestas.cryptmyfiles.fragments

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityCompat.finishAffinity
import androidx.core.app.ActivityCompat.getColor
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.michaelflisar.kotpreferences.core.interfaces.StorageSetting
import com.michaelflisar.materialpreferences.preferencescreen.*
import com.michaelflisar.materialpreferences.preferencescreen.bool.switch
import com.michaelflisar.materialpreferences.preferencescreen.choice.asChoiceListString
import com.michaelflisar.materialpreferences.preferencescreen.choice.singleChoice
import com.michaelflisar.materialpreferences.preferencescreen.classes.Badge
import com.michaelflisar.materialpreferences.preferencescreen.classes.asIcon
import com.michaelflisar.materialpreferences.preferencescreen.dependencies.Dependency
import com.michaelflisar.materialpreferences.preferencescreen.dependencies.asDependency
import com.michaelflisar.materialpreferences.preferencescreen.enums.NoIconVisibility
import com.michaelflisar.materialpreferences.preferencescreen.input.input
import com.michaelflisar.materialpreferences.preferencescreen.slider.slider
import com.michaelflisar.text.asText
import com.zestas.cryptmyfiles.activities.MainActivity
import com.zestas.cryptmyfiles.R
import com.zestas.cryptmyfiles.activities.AboutActivity
import com.zestas.cryptmyfiles.activities.ExportFilesActivity
import com.zestas.cryptmyfiles.activities.ImportFilesActivity
import com.zestas.cryptmyfiles.activities.SelectCustomDirectoryActivity
import com.zestas.cryptmyfiles.databinding.FragmentSettingsBinding
import com.zestas.cryptmyfiles.dataItemModels.ZenCryptSettingsModel
import com.zestas.cryptmyfiles.helpers.FileActionsHelper
import com.zestas.cryptmyfiles.helpers.IapHelper
import com.zestas.cryptmyfiles.helpers.SecureStorageHelper
import com.zestas.cryptmyfiles.helpers.ui.SnackBarHelper
import dev.skomlach.biometric.compat.*
import games.moisoni.google_iab.BillingConnector
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.nio.charset.Charset
import java.security.MessageDigest
import kotlin.system.exitProcess


class SettingsFragment : Fragment(R.layout.fragment_settings) {
    private val binding by viewBinding(FragmentSettingsBinding::bind)
    private lateinit var preferenceScreen: PreferenceScreen
    private lateinit var billingConnector: BillingConnector

    private val regexWithDot: Regex = "^\\.(([a-z]|[A-Z])+)\$".toRegex()
    private val regexWithoutDot: Regex = "^(([a-z]|[A-Z])+)\$".toRegex()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        billingConnector = IapHelper.initBillingConnector(activity as AppCompatActivity)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (ZenCryptSettingsModel.useDifferentDirectory.value && ZenCryptSettingsModel.differentDirectoryUri.value == "null") {
            lifecycleScope.launch {
                ZenCryptSettingsModel.useDifferentDirectory.update(false)
            }
        }

        preferenceScreen = initSettings(savedInstanceState)
        binding.upButton.setOnClickListener {
            preferenceScreen.onBackPressed()
        }
    }

    private fun initSettings(savedInstanceState: Bundle?): PreferenceScreen {

        // global settings to avoid
        // INFO:
        // some global settings can be overwritten per preference (e.g. bottomSheet yes/no)
        // other global settings can only be changed globally

        // following is optional!
        PreferenceScreenConfig.apply {
            bottomSheet = false                             // default: false
            maxLinesTitle = 1                               // default: 1
            maxLinesSummary = 3                             // default: 3
            noIconVisibility = NoIconVisibility.Invisible   // default: Invisible
            alignIconsWithBackArrow = false                 // default: false
        }

        // -----------------
        // 1) create screen(s)
        // -----------------

        val screen = screen {

            // set up screen
            state = savedInstanceState
            onScreenChanged = { subScreenStack, _ ->
                if (subScreenStack.isNotEmpty())
                    binding.upButton.visibility = View.VISIBLE
                else
                    binding.upButton.visibility = View.GONE
            }

            // set up settings (and sub settings)

            // -----------------
            // 1) test app settings (root level)
            // -----------------

            category {
                title = getString(R.string.support_development).asText()
            }

            if ( ZenCryptSettingsModel.isProUser.value ) {
                button {
                    title = "ZenCrypt Pro".asText()
                    summary = getString(R.string.zencrypt_pro_is_already_purchased).asText()
                    icon = R.drawable.cart_check.asIcon()
                    badge = Badge.Text(getString(R.string.pro_unlocked).asText(), ContextCompat.getColor(requireActivity(), R.color.green_active))
                    onClick = {
                        SnackBarHelper.showSnackBarLove(requireActivity() as AppCompatActivity, getString(R.string.you_have_already_purchased))
                    }
                }
            }
            else {
                button {
                    title = getString(R.string.go_pro).asText()
                    summary = getString(R.string.i_am_solo_developing).asText()
                    icon = R.drawable.ic_baseline_attach_money_24.asIcon()
                    badge = Badge.Text("Pro not Unlocked".asText(), Color.RED)
                    onClick = {
                        when {
                            billingConnector.isReady -> billingConnector.purchase(requireActivity(),"zencrypt_pro")
                            else -> SnackBarHelper.showSnackBarError(requireActivity() as AppCompatActivity, getString(R.string.google_play_billing_error))
                        }
                    }
                }
            }



            category {
                title = "App Style".asText()
            }

            switch(ZenCryptSettingsModel.darkTheme) {
                title = getString(R.string.dark_theme).asText()
                icon = R.drawable.ic_baseline_style_24.asIcon()
                summary = getString(R.string.choose_between_light_and_dark).asText()
                badge = Badge.Text("PRO".asText(), getColor(requireContext(), R.color.ZenCryptSecondaryLight))
                canChange = {
                    if ( !ZenCryptSettingsModel.isProUser.value )
                        SnackBarHelper.showSnackBarBuyPro(requireActivity() as AppCompatActivity)
                    ZenCryptSettingsModel.isProUser.value
                }
                onChanged = {
                    println("Dark Theme Settings Listener called: $it")
                    //recreate()
                    MainActivity.themeChanged()
                    AppCompatDelegate.setDefaultNightMode(if (it) AppCompatDelegate.MODE_NIGHT_YES else AppCompatDelegate.MODE_NIGHT_NO)
                }
            }

            category {
                title = getString(R.string.configuration).asText()
            }

            subScreen {
                title = getString(R.string.experimental_features).asText()
                summary = getString(R.string.enable_some_extra_experimental_features_keep_in_mind).asText()
                badge = Badge.Text("PRO".asText(), getColor(requireContext(), R.color.ZenCryptSecondaryLight))
                icon = R.drawable.flask_outline.asIcon()

                category {
                    title = getString(R.string.custom_directory).asText()
                }

                switch(ZenCryptSettingsModel.useDifferentDirectory) {
                    title = getString(R.string.use_custom_directory).asText()
                    summary = getString(R.string.change_zencrypts_working_directory).asText()
                    icon = R.drawable.folder_cog_outline.asIcon()
                    badge = Badge.Text("PRO".asText(), getColor(requireContext(), R.color.ZenCryptSecondaryLight))
                    canChange = {
                        if ( !ZenCryptSettingsModel.isProUser.value )
                            SnackBarHelper.showSnackBarBuyPro(requireActivity() as AppCompatActivity)
                        ZenCryptSettingsModel.isProUser.value
                    }
                    onChanged = {
                        if (it) SnackBarHelper.showSnackBarInfo(requireActivity() as AppCompatActivity, getString(R.string.select_a_custom_directory_below))
                        else {
                            lifecycleScope.launch(Dispatchers.IO)  {
                                ZenCryptSettingsModel.differentDirectoryUri.update("null")
                            }
                        }
                    }
                }

                button {
                    title = getString(R.string.select_custom_directory).asText()
                    summary = getString(R.string.only_local_directories_are_supported).asText()
//                    icon = R.drawable.folder_multiple_plus_outline.asIcon()
                    enabledDependsOn = ZenCryptSettingsModel.useDifferentDirectory.asDependency()
                    onClick = {
                        val intent = Intent(requireActivity(), SelectCustomDirectoryActivity::class.java)
                        startActivity(intent)
                    }
                }

                info {
//                    title = getString(R.string.important_info).asText()
                    icon = R.drawable.flash_triangle_outline.asIcon()
                    summary = getString(R.string.while_selecting_a_custom_directory_offers_convenience).asText()
                    enabledDependsOn = ZenCryptSettingsModel.useDifferentDirectory.asDependency()
                }

                category {
                    title = getString(R.string.iterations).asText()
                }


                switch(ZenCryptSettingsModel.useCustomNumberOfIterations) {
                    title = getString(R.string.set_custom_iterations).asText()
                    summary = getString(R.string.change_the_default_number_of_iterations).asText()
                    icon = R.drawable.repeat_variant.asIcon()
                    badge = Badge.Text("PRO".asText(), getColor(requireContext(), R.color.ZenCryptSecondaryLight))
                    canChange = {
                        if ( !ZenCryptSettingsModel.isProUser.value )
                            SnackBarHelper.showSnackBarBuyPro(requireActivity() as AppCompatActivity)
                        ZenCryptSettingsModel.isProUser.value
                    }
                    onChanged = {
                        if (!it) {
                            // reset default iteration number
                            lifecycleScope.launch {
                                ZenCryptSettingsModel.iterationsNumber.update(10000)
                                com.pvryan.easycrypt.Constants.setCustomIterationNumber(10000)
                            }
                        }
                    }
                }

                slider(ZenCryptSettingsModel.iterationsNumber) {
                    title = getString(R.string.iterations).asText()
                    summary = "Selected: %d | Default: 10000".asText()
                    enabledDependsOn = ZenCryptSettingsModel.useCustomNumberOfIterations.asDependency()
                    min = 7000
                    max = 20000
                    stepSize = 100
                    onChanged = {
                        com.pvryan.easycrypt.Constants.setCustomIterationNumber(it)
                    }
                }

                info {
                    icon = R.drawable.flash_triangle_outline.asIcon()
                    summary = getString(R.string.changing_the_number_of_iterations_will_not_affect_already_encrypted_files).asText()
                    enabledDependsOn = ZenCryptSettingsModel.useCustomNumberOfIterations.asDependency()
                }

            }

            switch(ZenCryptSettingsModel.vibration) {
                title = getString(R.string.enable_vibration).asText()
                icon = R.drawable.ic_baseline_vibrate_24.asIcon()
                summary = getString(R.string.vibrate_after_every_action).asText()
            }

            switch(ZenCryptSettingsModel.delete_unencrypted_on_exit) {
                title = getString(R.string.delete_unencrypted_files).asText()
                icon = R.drawable.delete_clock_outline.asIcon()
                summary = getString(R.string.delete_unencrypted_on_app_exit).asText()
                onChanged = {
                    if (it) {
                        val dialog =
                            AlertDialog.Builder(requireContext(), R.style.AlertDialogCustom)
                        dialog.setTitle(getString(R.string.warning))
                        dialog.setMessage(getString(R.string.enabling_this_option_requires_restart))
                        dialog.setCancelable(false)
                        dialog.setPositiveButton("I Understand") { dia, _ ->
                            dia.dismiss()
                            finishAffinity(requireActivity())
                            exitProcess(0)
                        }
                        dialog.setNegativeButton("Cancel") { dia, _ ->
                            lifecycleScope.launch(Dispatchers.IO) {
                                ZenCryptSettingsModel.delete_unencrypted_on_exit.update(false)
                            }
                            dia.dismiss()
                        }
                        dialog.show()
                    }
                    else {
                        val dialog =
                            AlertDialog.Builder(requireContext(), R.style.AlertDialogCustom)
                        dialog.setTitle(getString(R.string.warning))
                        dialog.setMessage(getString(R.string.restart_required))
                        dialog.setCancelable(false)
                        dialog.setPositiveButton("I Understand") { dia, _ ->
                            dia.dismiss()
                            finishAffinity(requireActivity())
                            exitProcess(0)
                        }
                        dialog.setNegativeButton("Later") { dia, _ ->
                            dia.dismiss()
                        }
                        dialog.show()
                    }
                }
            }

            val sortChoices = listOf(
                "Last Modified",
                "File Name"
            ).asChoiceListString()

            singleChoice(ZenCryptSettingsModel.sorting, sortChoices) {
                title = getString(R.string.file_sorting).asText()
                summary = getString(R.string.choose_how_you_want_to_sort_your_files).asText()
                icon = R.drawable.sort_ascending.asIcon()
            }

            val displayFileItemChoices = listOf(
                "Always Collapsed",
                "Always Expanded"
            ).asChoiceListString()

            singleChoice(ZenCryptSettingsModel.expandListItems, displayFileItemChoices) {
                title = getString(R.string.file_display_view).asText()
                summary = getString(R.string.choose_how_you_want_to_display_file_list_items).asText()
                icon = R.drawable.arrow_collapse_vertical.asIcon()
            }

            input(ZenCryptSettingsModel.extension) {
                title = getString(R.string.encrypted_file_extension).asText()
                icon = R.drawable.ic_baseline_text_fields_24.asIcon()
                summary = "Insert the preferable output file extension (Example: file.zen).\nCurrent: \"%s\"".asText()
                hint = getString(R.string.file_extension).asText()
                onChanged = {
                    when {
                        it.matches(regexWithDot) -> SnackBarHelper.showSnackBarCheck(requireActivity() as AppCompatActivity, "File extension changed to \"$it\"")
                        it.matches(regexWithoutDot) -> {
                            lifecycleScope.launch(Dispatchers.IO)  {
                                ZenCryptSettingsModel.extension.update(".$it")
                            }
                            SnackBarHelper.showSnackBarCheck(requireActivity() as AppCompatActivity, "File extension changed to \".$it\"")
                        }
                        else -> {
                            SnackBarHelper.showSnackBarInfo(requireActivity() as AppCompatActivity, getString(R.string.are_you_sure_you_typed_the_correct_file_extension))
                        }
                    }
                }
            }

            category {
                title = getString(R.string.fingerprint_auth).asText()
            }

            switch(ZenCryptSettingsModel.fingerprint_auth) {
                title = getString(R.string.enable_fingerprint_auth).asText()
                icon = R.drawable.ic_baseline_fingerprint_24.asIcon()
                summary = getString(R.string.enabling_fingerprint_auth).asText()
                badge = Badge.Text("PRO".asText(), getColor(requireContext(), R.color.ZenCryptSecondaryLight))
                canChange = {
                    if ( !ZenCryptSettingsModel.isProUser.value )
                        SnackBarHelper.showSnackBarBuyPro(requireActivity() as AppCompatActivity)
                    ZenCryptSettingsModel.isProUser.value
                }
                onChanged = {
                    if (it) SnackBarHelper.showSnackBarInfo(requireActivity() as AppCompatActivity, getString(R.string.set_a_custom_password_below))
                }
                enabledDependsOn = object : Dependency<Boolean> {
                    override suspend fun state(): Boolean {
                        val fingerprintRequest =
                            BiometricAuthRequest(BiometricApi.AUTO, BiometricType.BIOMETRIC_FINGERPRINT)

                        return BiometricManagerCompat.isHardwareDetected(fingerprintRequest) &&
                                BiometricManagerCompat.hasEnrolled(fingerprintRequest)
                    }

                    override val setting: StorageSetting<Boolean>
                        get() = ZenCryptSettingsModel.fingerprint_auth
                }
            }

            input(ZenCryptSettingsModel.custom_pass_placeholder) {
                title = getString(R.string.set_custom_password).asText()
                icon = R.drawable.ic_baseline_textbox_password.asIcon()
                summary = getString(R.string.any_password_is_valid).asText()
                hint = getString(R.string.enter_a_password).asText()
                enabledDependsOn = ZenCryptSettingsModel.fingerprint_auth.asDependency()
                onChanged = {
                    lifecycleScope.launch(Dispatchers.IO)  {
                        val secureStorageHelper = SecureStorageHelper()
                        //check if password is empty
                        if (it.trim() == "") {
                            ZenCryptSettingsModel.fingerprint_auth.update(false)
                            SnackBarHelper.showSnackBarError(requireActivity() as AppCompatActivity, getString(R.string.empty_password))
                        }
                        else
                            SnackBarHelper.showSnackBarCheck(requireActivity() as AppCompatActivity, getString(R.string.password_set))
                        //update the real password with user's input hash
                        //ZenCryptSettingsModel.custom_pass_hash.update(it.sha256()) -- DEPRECATED
                        secureStorageHelper.storeSecretP(activity as AppCompatActivity,
                            it.sha256().toByteArray(Charset.defaultCharset()))
                        //then set the placeholder text view to empty again
                        ZenCryptSettingsModel.custom_pass_placeholder.update("")
                    }
                }
                textInputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
            }

            category {
                title = getString(R.string.file_operations).asText()
            }

            button {
                title = getString(R.string.export_encrypted_files).asText()
                summary = getString(R.string.export_all_encrypted_files_on_selected_location).asText()
                icon = R.drawable.upload_lock.asIcon()
                onClick = {
                    val intent = Intent(requireActivity(), ExportFilesActivity::class.java)
                    startActivity(intent)
                }
            }

            button {
                title = getString(R.string.import_encrypted_files).asText()
                summary = getString(R.string.import_all_encrypted_files_from_selected_location).asText()
                icon = R.drawable.download_lock_outline.asIcon()
                onClick = {
                    val intent = Intent(requireActivity(), ImportFilesActivity::class.java)
                    startActivity(intent)
                }
            }

            button {
                title = getString(R.string.delete_all_stored_files).asText()
                summary = getString(R.string.delete_files_stored_within_the_app).asText()
                icon = R.drawable.delete_circle_outline.asIcon()
                onClick = {
                    FileActionsHelper.showDeleteAllFilesConfirmDialog(requireActivity() as AppCompatActivity)
                }
            }

            category {
                title = getString(R.string.about_zencrypt).asText()
            }

            button {
                title = getString(R.string.rate_zencrypt).asText()
                summary = getString(R.string.if_you_like_the_app).asText()
                icon = R.drawable.star_outline.asIcon()
                onClick = {
                    try {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.zestas.cryptmyfiles")))
                    } catch (e1: ActivityNotFoundException) {
                        try {
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.zestas.cryptmyfiles")))
                        } catch (e2: ActivityNotFoundException) {
                            SnackBarHelper.showSnackBarError(requireActivity() as AppCompatActivity, "Cannot open link.")
                        }
                    }
                }
            }

            button {
                title = getString(R.string.contact_me).asText()
                summary = getString(R.string.send_me_an_email).asText()
                icon = R.drawable.mail_at.asIcon()
                onClick = {
                    val selectorIntent = Intent(Intent.ACTION_SENDTO)
                    selectorIntent.data = Uri.parse("mailto:")

                    val emailIntent = Intent(Intent.ACTION_SEND)
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf("oz.zencrypt@gmail.com"))
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "ZenCrypt Feedback")
                    emailIntent.selector = selectorIntent

                    startActivity(Intent.createChooser(emailIntent, "ZenCrypt Feedback"))
                }
            }

            button {
                title = getString(R.string.source_code).asText()
                summary = getString(R.string.zencrypt_is_now_open_source).asText()
                icon = R.drawable.gitlab.asIcon()
                onClick = {
                    try {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/o.zestas/zencrypt")))
                    } catch (e: ActivityNotFoundException) {
                        SnackBarHelper.showSnackBarError(requireActivity() as AppCompatActivity, "Cannot open link.")
                    }
                }
            }

            button {
                title = getString(R.string.about).asText()
                summary = getString(R.string.zencrypt_changelog_and_libraries).asText()
                icon = R.drawable.info_outline.asIcon()
                onClick = {
                    val intent = Intent(requireActivity(), AboutActivity::class.java)
                    startActivity(intent)
                }
            }

        }
        screen.bind(binding.rvSettings, this)
        return screen
    }

    override fun onDestroyView() {
        val recyclerView: RecyclerView = binding.rvSettings
        recyclerView.adapter = null
        super.onDestroyView()
    }

    override fun onDestroy() {
        super.onDestroy()
        billingConnector.release()
    }

    private fun String.sha256(): String {
        return MessageDigest
            .getInstance("SHA-256")
            .digest(this.toByteArray())
            .fold("") { str, it -> str + "%02x".format(it) }
    }
}