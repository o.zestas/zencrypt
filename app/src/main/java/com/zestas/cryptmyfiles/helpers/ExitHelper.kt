package com.zestas.cryptmyfiles.helpers

import android.app.Activity
import android.content.Intent

class ExitHelper {
    companion object {
        fun exit(activity: Activity) {
            val _intentOBJ = Intent(Intent.ACTION_MAIN)
            _intentOBJ.addCategory(Intent.CATEGORY_HOME)
            _intentOBJ.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            _intentOBJ.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            activity.startActivity(_intentOBJ)
        }
    }
}