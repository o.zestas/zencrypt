package com.zestas.cryptmyfiles.helpers.ui

import android.content.Intent
import android.graphics.Color
import android.transition.TransitionManager
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import com.google.android.material.color.MaterialColors
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.transition.platform.MaterialArcMotion
import com.google.android.material.transition.platform.MaterialContainerTransform
import com.ismaeldivita.chipnavigation.ChipNavigationBar
import com.zestas.cryptmyfiles.R
import com.zestas.cryptmyfiles.activities.ActionActivity
import com.zestas.cryptmyfiles.constants.ZenCryptUtils
import com.zestas.cryptmyfiles.dataItemModels.ZenCryptSettingsModel


class FabHelper {
    companion object {

        fun init(activity: AppCompatActivity) {
            initFabListener(activity)
            initOnClickListeners(activity)
        }
        private fun initFabListener(activity: AppCompatActivity) {
            val fab: ExtendedFloatingActionButton = activity.findViewById(R.id.fab)
            val fabContainer: LinearLayout = activity.findViewById(R.id.fab_container)
            val fabScrim: FrameLayout = activity.findViewById(R.id.fab_scrim)
            fab.setOnClickListener {
                val transition = buildContainerTransformation(activity)

                transition.startView = fab
                transition.endView = fabContainer

                transition.addTarget(fabContainer)

                TransitionManager.beginDelayedTransition(
                    activity.findViewById(android.R.id.content),
                    transition
                )
                fabContainer.visibility = View.VISIBLE
                fabScrim.visibility = View.VISIBLE

                fab.visibility = View.INVISIBLE
            }

            fabScrim.setOnClickListener {
                val transition = buildContainerTransformation(activity)

                transition.startView = fabContainer
                transition.endView = fab

                transition.addTarget(fab)

                TransitionManager.beginDelayedTransition(
                    activity.findViewById(android.R.id.content),
                    transition
                )
                fabContainer.visibility = View.INVISIBLE
                fabScrim.visibility = View.INVISIBLE

                fab.visibility = View.VISIBLE
            }
        }

        private fun buildContainerTransformation(activity: AppCompatActivity) =
            MaterialContainerTransform().apply {
                containerColor = MaterialColors.getColor(
                    activity.window.decorView.findViewById(android.R.id.content),
                    com.google.android.material.R.attr.colorSecondary
                )
                scrimColor = Color.TRANSPARENT
                duration = 300
                interpolator = FastOutSlowInInterpolator()
                fadeMode = MaterialContainerTransform.FADE_MODE_IN
                setAllContainerColors(Color.TRANSPARENT)
                pathMotion = MaterialArcMotion()
            }

        private fun initOnClickListeners(activity: AppCompatActivity) {
            val fabScrim: FrameLayout = activity.findViewById(R.id.fab_scrim)
            val fabActionEncrypt: Button = activity.findViewById(R.id.btn_encrypt_file)
            val fabActionEncryptMultiple: Button = activity.findViewById(R.id.btn_encrypt_multiple)
            val fabActionDecrypt: Button = activity.findViewById(R.id.btn_decrypt_file)
            val fabActionDecryptMultiple : Button = activity.findViewById(R.id.btn_decrypt_multiple)

            fabActionEncrypt.setOnClickListener {
                fabScrim.performClick()
                val intent = Intent(activity, ActionActivity::class.java)
                intent.putExtra(ZenCryptUtils.REQUEST_CODE, ZenCryptUtils.FILE_PICK)
                intent.putExtra(ZenCryptUtils.ACTION_CODE, ZenCryptUtils.ACTION_ENCRYPT)
                intent.putExtra(ZenCryptUtils.REPLACE_CODE, ZenCryptUtils.REPLACE_WITH_ENCRYPTED)
                activity.startActivity(intent)
            }

            fabActionEncryptMultiple.setOnClickListener {
                if (ZenCryptSettingsModel.isProUser.value) {
                    fabScrim.performClick()
                    val intent = Intent(activity, ActionActivity::class.java)
                    intent.putExtra(
                        ZenCryptUtils.REQUEST_CODE,
                        ZenCryptUtils.FILE_PICK_MULTIPLE
                    )
                    intent.putExtra(
                        ZenCryptUtils.ACTION_CODE,
                        ZenCryptUtils.ACTION_ENCRYPT_MULTIPLE
                    )
                    intent.putExtra(ZenCryptUtils.REPLACE_CODE, ZenCryptUtils.REPLACE_WITH_ENCRYPTED)
                    activity.startActivity(intent)
                } else {
                    fabScrim.performClick()
                    SnackBarHelper.showSnackBarBuyPro(activity)
                }
            }

            fabActionDecryptMultiple.setOnClickListener {
                if (ZenCryptSettingsModel.isProUser.value) {
                    fabScrim.performClick()
                    val intent = Intent(activity, ActionActivity::class.java)
                    intent.putExtra(
                        ZenCryptUtils.REQUEST_CODE,
                        ZenCryptUtils.FILE_PICK_MULTIPLE
                    )
                    intent.putExtra(
                        ZenCryptUtils.ACTION_CODE,
                        ZenCryptUtils.ACTION_DECRYPT_MULTIPLE
                    )
                    intent.putExtra(ZenCryptUtils.REPLACE_CODE, ZenCryptUtils.REPLACE_WITH_DECRYPTED)
                    activity.startActivity(intent)
                } else {
                    fabScrim.performClick()
                    SnackBarHelper.showSnackBarBuyPro(activity)
                }
            }

            fabActionDecrypt.setOnClickListener {
                fabScrim.performClick()
                val intent = Intent(activity, ActionActivity::class.java)
                intent.putExtra(ZenCryptUtils.REQUEST_CODE, ZenCryptUtils.FILE_PICK)
                intent.putExtra(ZenCryptUtils.ACTION_CODE, ZenCryptUtils.ACTION_DECRYPT)
                intent.putExtra(ZenCryptUtils.REPLACE_CODE, ZenCryptUtils.REPLACE_WITH_DECRYPTED)
                activity.startActivity(intent)
            }
        }

    }
}