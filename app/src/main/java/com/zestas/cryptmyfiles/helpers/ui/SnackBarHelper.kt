package com.zestas.cryptmyfiles.helpers.ui

import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.ismaeldivita.chipnavigation.ChipNavigationBar
import com.zestas.cryptmyfiles.R
import com.zestas.cryptmyfiles.activities.MainActivity

class SnackBarHelper {
    companion object {
        fun showSnackBarInfo(activity: AppCompatActivity, message: String) {
            if (activity !is MainActivity) {
                Snackbar.make(activity.findViewById(R.id.content), message, Snackbar.LENGTH_LONG)
                    .setTextColor(ContextCompat.getColor(activity, R.color.white))
                    .setBackgroundTint(ContextCompat.getColor(activity, R.color.fabMenuColorLight))
                    .show()
            }
            else {
                Snackbar.make(activity.findViewById(R.id.extended_fab_coordinator), message, Snackbar.LENGTH_LONG)
                    .setTextColor(ContextCompat.getColor(activity, R.color.white))
                    .setBackgroundTint(ContextCompat.getColor(activity, R.color.fabMenuColorLight))
                    .show()
            }
        }

        fun showSnackBarCheck(activity: AppCompatActivity, message: String) {
            if (activity !is MainActivity) {
                Snackbar.make(activity.findViewById(R.id.content), message, Snackbar.LENGTH_LONG)
                    .setTextColor(ContextCompat.getColor(activity, R.color.white))
                    .setBackgroundTint(ContextCompat.getColor(activity, R.color.green_active))
                    .show()
            }
            else {
                Snackbar.make(activity.findViewById(R.id.extended_fab_coordinator), message, Snackbar.LENGTH_LONG)
                    .setTextColor(ContextCompat.getColor(activity, R.color.white))
                    .setBackgroundTint(ContextCompat.getColor(activity, R.color.green_active))
                    .show()
            }
        }

        fun showSnackBarError(activity: AppCompatActivity, message: String) {
            if (activity !is MainActivity) {
                Snackbar.make(activity.findViewById(R.id.content), message, Snackbar.LENGTH_LONG)
                    .setTextColor(ContextCompat.getColor(activity, R.color.white))
                    .setBackgroundTint(ContextCompat.getColor(activity, R.color.red_active))
                    .show()
            }
            else {
                Snackbar.make(activity.findViewById(R.id.extended_fab_coordinator), message, Snackbar.LENGTH_LONG)
                    .setTextColor(ContextCompat.getColor(activity, R.color.white))
                    .setBackgroundTint(ContextCompat.getColor(activity, R.color.red_active))
                    .show()
            }
        }

        fun showSnackBarLove(activity: AppCompatActivity, message: String) {
            Snackbar.make(activity.findViewById(R.id.extended_fab_coordinator), message, Snackbar.LENGTH_LONG)
                .setBackgroundTint(ContextCompat.getColor(activity, R.color.colorAccent))
                .setTextColor(ContextCompat.getColor(activity, R.color.white))
                .show()
        }

        fun showSnackBarBuyPro(activity: AppCompatActivity) {
            Snackbar.make(activity.findViewById(R.id.extended_fab_coordinator), "This is a PRO feature.", Snackbar.LENGTH_LONG)
                .setAction("BUY") {
                    val menu = activity.findViewById<ChipNavigationBar>(R.id.bottom_menu)
//                    FragmentHelper.replaceFragmentWithDelay(SettingsFragment(),0)
                    if (menu.getSelectedItemId() != R.id.settings)
                        menu.setItemSelected(R.id.settings)
                }
                .setTextColor(ContextCompat.getColor(activity, R.color.white))
                .setActionTextColor(ContextCompat.getColor(activity,R.color.white))
                .setBackgroundTint(ContextCompat.getColor(activity, R.color.red_active))
                .show()
        }
    }
}
