package com.zestas.cryptmyfiles.helpers.autodeletefiles

import androidx.appcompat.app.AppCompatActivity
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.*
import com.zestas.cryptmyfiles.constants.ZenCryptUtils
import java.io.File

class ZencryptLifecycleObserver(var activity: AppCompatActivity) : LifecycleEventObserver {

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        when (event) {
            Lifecycle.Event.ON_RESUME -> {}
            Lifecycle.Event.ON_PAUSE -> {}
            Lifecycle.Event.ON_DESTROY-> {
                if (ZenCryptUtils.isUsingCustomDirectory()) {
                    val unencryptedDir = ZenCryptUtils.decryptedFilesDirExternal(activity)
                    for (documentFile: DocumentFile in unencryptedDir.listFiles()) {
                        if (!documentFile.isDirectory)
                            documentFile.delete()
                    }
                }
                else {
                    val unencryptedDir = ZenCryptUtils.decryptedFilesDirInternal(activity)
                    for (file: File in unencryptedDir.listFiles()!!) {
                        if (!file.isDirectory)
                            file.delete()
                    }

                }
            }
            Lifecycle.Event.ON_CREATE -> {}
            Lifecycle.Event.ON_START -> {}
            Lifecycle.Event.ON_STOP -> {
/*                val unencryptedDir = ZenCryptUtils.decryptedFilesDir(activity)
                for (documentFile: DocumentFile in unencryptedDir.listFiles()) {
                    if (!documentFile.isDirectory)
                        documentFile.delete()
                }*/
            }
            Lifecycle.Event.ON_ANY -> {}
        }
    }
}

