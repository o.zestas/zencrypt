package com.zestas.cryptmyfiles.helpers

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.zestas.cryptmyfiles.constants.ZenCryptUtils
import com.zestas.cryptmyfiles.dataItemModels.ZenCryptSettingsModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.nio.charset.Charset

class UpdateMigrationHelper {
    companion object {
        fun checkForFingerprintAuthStorageMigration() : Boolean {
            return ZenCryptSettingsModel.versionCode.value < ZenCryptUtils.ZENCRYPT_VERSION_4_3
                    && ZenCryptSettingsModel.custom_pass_hash.value != "@@DEPRECATED@@"
        }
        fun performFingerprintAuthStorageMigration(activity: AppCompatActivity) {
            Log.d("Zen", "Performing FingerprintAuthStorageMigration")
            val secureStorageHelper = SecureStorageHelper()
            val bytes: ByteArray = ZenCryptSettingsModel.custom_pass_hash.value.toByteArray(
                Charset.defaultCharset())
            secureStorageHelper.storeSecretP(activity, bytes)
            activity.lifecycleScope.launch(Dispatchers.IO)  {
                ZenCryptSettingsModel.custom_pass_hash.update("@@DEPRECATED@@")
            }
        }

    }
}