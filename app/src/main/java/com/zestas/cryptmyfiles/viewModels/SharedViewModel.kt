package com.zestas.cryptmyfiles.viewModels

import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zestas.cryptmyfiles.events.SnackBarEvent

class SharedViewModel : ViewModel() {
    val infoMessage = MutableLiveData<SnackBarEvent<String>?>()
    val checkMessage = MutableLiveData<SnackBarEvent<String>?>()
    val errorMessage = MutableLiveData<SnackBarEvent<String>?>()
    val selectedItemId = MutableLiveData<Int?>()
    private val _fragmentTransactionEvent = MutableLiveData<FragmentTransactionEvent>()
    val fragmentTransactionEvent: LiveData<FragmentTransactionEvent> = _fragmentTransactionEvent

    fun replaceFragmentWithDelay(fragment: Fragment, timeMillis: Long = 300) {
        _fragmentTransactionEvent.value = FragmentTransactionEvent(fragment, timeMillis)
    }

    fun clearData() {
        infoMessage.value = null
        checkMessage.value = null
        errorMessage.value = null
        selectedItemId.value = null
    }

    data class FragmentTransactionEvent(val fragment: Fragment, val delay: Long)
}