package com.zestas.cryptmyfiles

import android.app.Application
import androidx.lifecycle.ViewModelProvider
import com.zestas.cryptmyfiles.viewModels.SharedViewModel

class ZenCrypt : Application() {
    val sharedViewModel: SharedViewModel by lazy {
        ViewModelProvider.AndroidViewModelFactory.getInstance(this).create(SharedViewModel::class.java)
    }
}