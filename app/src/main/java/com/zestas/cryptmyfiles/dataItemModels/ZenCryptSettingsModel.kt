package com.zestas.cryptmyfiles.dataItemModels

import com.michaelflisar.kotpreferences.core.SettingsModel
import com.michaelflisar.kotpreferences.datastore.DataStoreStorage

object ZenCryptSettingsModel : SettingsModel(DataStoreStorage(name = "zencrypt_settings")) {
    //-------
    val darkTheme by boolPref(false)
    val vibration by boolPref(true)
    val extension by stringPref(".zen")
    val fingerprint_auth by boolPref(false)
    val fingerprint_auth_secret by stringPref("")
    // custom_pass_hash is deprecated as of version 4.3, and will be removed
    // in future updates!
    val custom_pass_hash by stringPref("")
    val custom_pass_placeholder by stringPref("")
    val sorting by intPref(0)
    val expandListItems by intPref(0)
    val delete_unencrypted_on_exit by boolPref(false)
    val isProUser by boolPref(false)
    val versionCode by intPref(-1)
    //------- experimental
    val useDifferentDirectory by boolPref(false)
    val differentDirectoryUri by stringPref("null")
    val useCustomNumberOfIterations by boolPref(false)
    val iterationsNumber by intPref(10000)
    //-------
}