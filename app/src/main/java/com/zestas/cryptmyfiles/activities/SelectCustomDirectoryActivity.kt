package com.zestas.cryptmyfiles.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.lifecycleScope
import com.zestas.cryptmyfiles.R
import com.zestas.cryptmyfiles.ZenCrypt
import com.zestas.cryptmyfiles.constants.ZenCryptUtils
import com.zestas.cryptmyfiles.dataItemModels.ZenCryptSettingsModel
import com.zestas.cryptmyfiles.events.SnackBarEvent
import com.zestas.cryptmyfiles.viewModels.SharedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileInputStream
import java.io.OutputStream


class SelectCustomDirectoryActivity : AppCompatActivity() {
    private val cbMoveEncryptedFiles by lazy { findViewById<CheckBox>(R.id.move_encrypted_files_checkbox) }
    private val cbMoveDecryptedFiles by lazy { findViewById<CheckBox>(R.id.move_decrypted_files_checkbox) }
    private val cbLayout by lazy { findViewById<LinearLayout>(R.id.checkBoxLayout) }
    private val indeterminateProgressLayout by lazy { findViewById<LinearLayout>(R.id.indeterminate_progress_layout_custom_directory) }
    private val customDirectoryLayout by lazy { findViewById<LinearLayout>(R.id.custom_directory_layout) }
    private val tvDocumentTreeLocation by lazy { findViewById<TextView>(R.id.tvDocumentTreeLocation) }
    private val buttonCancel by lazy { findViewById<Button>(R.id.button_cancel_custom_directory) }
    private val buttonOk by lazy { findViewById<Button>(R.id.button_ok_custom_directory) }
    private var pickedDir: DocumentFile? = null
    private var pickedUri: Uri? = null
    private lateinit var sharedViewModel: SharedViewModel

    private val result =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val treeUri: Uri? = result.data?.data
                contentResolver.takePersistableUriPermission(
                    treeUri!!,
                    Intent.FLAG_GRANT_READ_URI_PERMISSION or
                            Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                )

                pickedUri = treeUri
                pickedDir = DocumentFile.fromTreeUri(this, treeUri)
                tvDocumentTreeLocation.text = pickedDir?.name
            }
            else
                finish()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedViewModel = (application as ZenCrypt).sharedViewModel
        setContentView(R.layout.activity_select_custom_directory)
        setFinishOnTouchOutside(false)
        initButtonListeners()

        onBackPressedDispatcher.addCallback(object: OnBackPressedCallback(true) {
            /* override back pressing */
            override fun handleOnBackPressed() {
                finish()
            }
        })

        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
        result.launch(intent)
    }

    private fun initButtonListeners() {
        buttonCancel.setOnClickListener {
            finish()
        }

        buttonOk.setOnClickListener {
            customDirectoryLayout.visibility = View.GONE
            cbLayout.visibility = View.GONE
            indeterminateProgressLayout.visibility = View.VISIBLE

            var newEncryptedDir: DocumentFile?
            var newDecryptedDir: DocumentFile?
            var oldEncryptedDirAsFile: File? = null
            var oldEncryptedDirAsDocumentFile: DocumentFile? = null
            var oldDecryptedDirAsFile: File? = null
            var oldDecryptedDirAsDocumentFile: DocumentFile? = null

            if (pickedDir != null) {
                disableButtons()
                if (ZenCryptUtils.isUsingCustomDirectory()) {
                    oldEncryptedDirAsDocumentFile =
                        ZenCryptUtils.encryptedFilesDirExternal(this@SelectCustomDirectoryActivity)
                    oldDecryptedDirAsDocumentFile =
                        ZenCryptUtils.decryptedFilesDirExternal(this@SelectCustomDirectoryActivity)
                }
                else {
                    oldEncryptedDirAsFile =
                        ZenCryptUtils.encryptedFilesDirInternal(this@SelectCustomDirectoryActivity)
                    oldDecryptedDirAsFile =
                        ZenCryptUtils.decryptedFilesDirInternal(this@SelectCustomDirectoryActivity)
                }

                lifecycleScope.launch {
                    newEncryptedDir = pickedDir!!.findFile("encrypted")
                    if (newEncryptedDir == null)
                        newEncryptedDir = pickedDir!!.createDirectory("encrypted")

                    newDecryptedDir = pickedDir!!.findFile("decrypted")
                    if (newDecryptedDir == null)
                        newDecryptedDir = pickedDir!!.createDirectory("decrypted")

                    ZenCryptSettingsModel.differentDirectoryUri.update(pickedUri!!.toString())

                    // copy encrypted files
                    // Check if the previously used dir was already an external dir,
                    // and then use DocumentFile approach.
                    // Else, i.e. the default internal dir is used,
                    // use the regular File approach
                    if (cbMoveEncryptedFiles.isChecked && newEncryptedDir != null) {
                        if (oldEncryptedDirAsDocumentFile != null) {
                            withContext(Dispatchers.IO) {
                                for (file in oldEncryptedDirAsDocumentFile.listFiles()) {
                                    if (!file.isDirectory) {
                                        Log.d(
                                            "Zen",
                                            "Custom Directory: moving encrypted docFile" + file.name
                                        )
                                        val newFile =
                                            newEncryptedDir!!.createFile(
                                                "application/unknown",
                                                file.name!!
                                            )
                                        if (newFile != null && newFile.canWrite()) {
                                            val out: OutputStream? =
                                                contentResolver.openOutputStream(newFile.uri)
                                            contentResolver.openInputStream(file.uri).use { fis ->
                                                val buffer = ByteArray(1024)
                                                var len: Int
                                                while (fis!!.read(buffer).also { len = it } != -1) {
                                                    out?.write(buffer, 0, len)
                                                }
                                            }
                                            out?.close()
                                        } else
                                            Log.d("Zen", "Exporting File " + file.name + " FAILED")
                                    }
                                }
                            }
                        }
                        else if (oldEncryptedDirAsFile != null) {
                            withContext(Dispatchers.IO) {
                                for (file in oldEncryptedDirAsFile.listFiles()!!) {
                                    if (!file.isDirectory) {
                                        Log.d(
                                            "Zen",
                                            "Custom Directory: moving encrypted regular file" + file.name
                                        )
                                        val newFile =
                                            newEncryptedDir!!.createFile("application/unknown", file.name)
                                        if (newFile != null && newFile.canWrite()) {
                                            val out: OutputStream? = contentResolver.openOutputStream(newFile.uri)
                                            FileInputStream(file).use { fis ->
                                                val buffer = ByteArray(1024)
                                                var len: Int
                                                while (fis.read(buffer).also { len = it } != -1) {
                                                    out?.write(buffer, 0, len)
                                                }
                                            }
                                            out?.close()
                                        } else
                                            Log.d("Zen", "Custom Directory: moving encrypted regular file" + file.name + " FAILED")
                                    }
                                }
                            }
                        }
                    }

                    // copy decrypted files
                    // Check if the previously used dir was already an external dir,
                    // and then use DocumentFile approach.
                    // Else, i.e. the default internal dir is used,
                    // use the regular File approach
                    if (cbMoveDecryptedFiles.isChecked && newDecryptedDir != null) {
                        if (oldDecryptedDirAsDocumentFile != null) {
                            withContext(Dispatchers.IO) {
                                for (file in oldDecryptedDirAsDocumentFile.listFiles()) {
                                    if (!file.isDirectory) {
                                        Log.d(
                                            "Zen",
                                            "Custom Directory: moving decrypted docFile" + file.name
                                        )
                                        val newFile =
                                            newDecryptedDir!!.createFile(
                                                "application/unknown",
                                                file.name!!
                                            )
                                        if (newFile != null && newFile.canWrite()) {
                                            val out: OutputStream? =
                                                contentResolver.openOutputStream(newFile.uri)
                                            contentResolver.openInputStream(file.uri).use { fis ->
                                                val buffer = ByteArray(1024)
                                                var len: Int
                                                while (fis!!.read(buffer).also { len = it } != -1) {
                                                    out?.write(buffer, 0, len)
                                                }
                                            }
                                            out?.close()
                                        } else
                                            Log.d("Zen", "Custom Directory: moving decrypted docFile" + file.name + " FAILED")
                                    }
                                }
                            }
                        }
                        else if (oldDecryptedDirAsFile != null) {
                            withContext(Dispatchers.IO) {
                                for (file in oldDecryptedDirAsFile.listFiles()!!) {
                                    if (!file.isDirectory) {
                                        Log.d(
                                            "Zen",
                                            "Custom Directory: moving decrypted regular file" + file.name
                                        )
                                        val newFile =
                                            newDecryptedDir!!.createFile("application/unknown", file.name)
                                        if (newFile != null && newFile.canWrite()) {
                                            val out: OutputStream? = contentResolver.openOutputStream(newFile.uri)
                                            FileInputStream(file).use { fis ->
                                                val buffer = ByteArray(1024)
                                                var len: Int
                                                while (fis.read(buffer).also { len = it } != -1) {
                                                    out?.write(buffer, 0, len)
                                                }
                                            }
                                            out?.close()
                                        } else
                                            Log.d("Zen", "Custom Directory: moving decrypted regular file" + file.name + " FAILED")
                                    }
                                }
                            }
                        }
                    }
                    sharedViewModel.infoMessage.postValue(SnackBarEvent(pickedDir.toString()))
                    finish()
                }
            }
        }
    }

    private fun disableButtons() {
        buttonCancel.isEnabled = false
        buttonCancel.alpha = 0.5f
        buttonOk.isEnabled = false
        buttonOk.alpha = 0.5f
    }
}