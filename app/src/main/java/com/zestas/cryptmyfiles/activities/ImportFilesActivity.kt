package com.zestas.cryptmyfiles.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.lifecycleScope
import com.zestas.cryptmyfiles.R
import com.zestas.cryptmyfiles.ZenCrypt
import com.zestas.cryptmyfiles.constants.ZenCryptUtils
import com.zestas.cryptmyfiles.dataItemModels.ZenCryptSettingsModel
import com.zestas.cryptmyfiles.events.SnackBarEvent
import com.zestas.cryptmyfiles.viewModels.SharedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.OutputStream


class ImportFilesActivity : AppCompatActivity() {
    private val indeterminateProgressLayout by lazy { findViewById<LinearLayout>(R.id.indeterminate_progress_layout_import) }
    private val layoutImportLocation by lazy { findViewById<LinearLayout>(R.id.import_location_layout) }
    private val tvDocumentTreeLocation by lazy { findViewById<TextView>(R.id.tvDocumentTreeLocation) }
    private val buttonCancel by lazy { findViewById<Button>(R.id.button_cancel_import) }
    private val buttonStartImport by lazy { findViewById<Button>(R.id.button_start_import) }
    private var pickedDir: DocumentFile? = null
    private lateinit var sharedViewModel: SharedViewModel

    private val result =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val treeUri: Uri? = result.data?.data
                contentResolver.takePersistableUriPermission(
                    treeUri!!,
                    Intent.FLAG_GRANT_READ_URI_PERMISSION or
                            Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                )

                pickedDir = DocumentFile.fromTreeUri(this, treeUri)
                tvDocumentTreeLocation.text = pickedDir?.name
            }
            else
                finish()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedViewModel = (application as ZenCrypt).sharedViewModel
        setContentView(R.layout.activity_import_files)
        setFinishOnTouchOutside(false)
        initButtonListeners()

        onBackPressedDispatcher.addCallback(object: OnBackPressedCallback(true) {
            /* override back pressing */
            override fun handleOnBackPressed() {
                if (!indeterminateProgressLayout.isVisible)
                    finish()
            }
        })

        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
        result.launch(intent)
    }

    private fun initButtonListeners() {
        buttonCancel.setOnClickListener {
            finish()
        }

        buttonStartImport.setOnClickListener {
            if (pickedDir != null) {
                disableButtons()
                layoutImportLocation.visibility = View.GONE
                indeterminateProgressLayout.visibility = View.VISIBLE
                lifecycleScope.launch {
                    if (ZenCryptUtils.isUsingCustomDirectory()) {
                        val encryptedFilesDir =
                            ZenCryptUtils.encryptedFilesDirExternal(this@ImportFilesActivity)
                        withContext(Dispatchers.IO) {
                            for (file in pickedDir!!.listFiles()) {
                                if (!file.isDirectory && file.name?.endsWith(ZenCryptSettingsModel.extension.value) == true) {
                                    Log.d("Zen", "Importing File " + file.name)

                                    val newFile = encryptedFilesDir.createFile(
                                        "application/unknown",
                                        file.name!!
                                    )

                                    if (newFile!!.canWrite()) {
                                        val out: OutputStream? =
                                            contentResolver.openOutputStream(newFile.uri)
                                        contentResolver.openInputStream(file.uri).use { fis ->
                                            val buffer = ByteArray(1024)
                                            var len: Int
                                            if (fis != null) {
                                                while (fis.read(buffer).also { len = it } != -1) {
                                                    out?.write(buffer, 0, len)
                                                }
                                            }
                                        }
                                        out?.close()
                                    } else
                                        Log.d("Zen", "Importing File " + file.name + " FAILED")
                                }
                            }
                        }
                    }
                    else {
                        val encryptedFilesDir =
                            ZenCryptUtils.encryptedFilesDirInternal(this@ImportFilesActivity)
                        withContext(Dispatchers.IO) {
                            for (file in pickedDir!!.listFiles()) {
                                if (!file.isDirectory && file.name?.endsWith(ZenCryptSettingsModel.extension.value) == true) {
                                    Log.d("Zen", "Importing File " + file.name)
                                    val filePath = encryptedFilesDir.absolutePath +
                                            File.separator + file.name
                                    val newFile = File(filePath)
                                    newFile.createNewFile()

                                    if (newFile.canWrite()) {
                                        val out: OutputStream? = contentResolver.openOutputStream(Uri.fromFile(newFile))
                                        contentResolver.openInputStream(file.uri).use { fis ->
                                            val buffer = ByteArray(1024)
                                            var len: Int
                                            if (fis != null) {
                                                while (fis.read(buffer).also { len = it } != -1) {
                                                    out?.write(buffer, 0, len)
                                                }
                                            }
                                        }
                                        out?.close()
                                    } else
                                        Log.d("Zen", "Importing File " + file.name + " FAILED")
                                }
                            }
                        }

                    }
                    sharedViewModel.checkMessage.postValue(SnackBarEvent(getString(R.string.file_import_completed)))
                    finish()
                }
            }
        }
    }

    private fun disableButtons() {
        buttonCancel.isEnabled = false
        buttonCancel.alpha = 0.5f
        buttonStartImport.isEnabled = false
        buttonStartImport.alpha = 0.5f
    }
}